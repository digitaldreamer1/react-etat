import React, { useEffect, useState } from "react";
import Profile from "./Profile";
import "./index.css";

function App() {
  const [timer, setTimer] = useState(0); 
  const [show, setShow] = useState(true);
  const [person, setPerson] = useState({
    fullname: "Jean Marc",
    bio: "Informaticien",
    img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRGAP-nnOJ4H9QG0q3zlS2JfN2B78LB2Rb-DA&s",
    description: "Software Developer, passionné par l'informatique surtout la branche de programmation, mon reve est de lancer des SAAS à succes en creant mon start-up",
  });

  useEffect(() => {
    const interval = setInterval(() => {
      setTimer(prevTimer => prevTimer + 1);
    }, 1000);

    return () => clearInterval(interval);
  }, []); 

  return (
    <div className="App">
      <header className="bg-green-800 text-white text-center py-4">
        <h1 className="text-2xl font-bold">Profile Personne</h1>
      </header>
      <main className="p-8">
        {show && <Profile Person={person} />}
        <p className="text-center font-bold" >Temps écoulé depuis le montage : {timer} secondes</p> 
      </main>
      <div className="flex justify-center mt-4">
  <button
    onClick={() => setShow(!show)}
    className="bg-blue-500 hover:bg-gray-700 tex-red font-bold py-2 px-4 rounded"
  >
    Afficher
  </button>
</div>

    </div>
  );
}

export default App;
