import React from "react";

const Profile = ({ Person }) => {
  const { fullname, bio, img, description } = Person;

  return (
    <div className="max-w-sm mx-auto bg-white shadow-lg rounded-lg overflow-hidden">
      <div className="p-4">
        <h2 className="text-xl font-bold">{` ${fullname}`}</h2>
        <h2 className="text-xl font-bold">{`${bio}`}</h2>

        <p className="text-gray-600">{description}</p>
      </div>
      <img
        className="w-full h-48 object-cover"
        src={img}
        alt={`${fullname} `}
      />
    </div>
  );
};

export default Profile;
